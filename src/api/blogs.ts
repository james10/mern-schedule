import { Post } from "@prisma/client";

export const fetchPosts = async (searchTerm: string): Promise<Post[]> => {
  const response = await fetch('/api/posts?s=' + searchTerm);
  const data = await response.json() as { posts: Post[] };

  return data.posts.map((post: Post) => ({
    ...post
  }));
}