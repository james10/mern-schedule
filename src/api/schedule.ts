import { type Schedule } from "@prisma/client";

export interface InputSchedule {
  id?: string | null;
  title: string;
  description: string;
  startDate?: string | null;
  dueDate?: string | null;
};

export const fetchSchedules = async (): Promise<Schedule[]> => {
  const response = await fetch('/api/schedule');
  const data = await response.json() as { schedules: Schedule[] };

  return data.schedules.map((schedule: Schedule) => ({
    id: schedule.id,
    title: schedule.title,
    description: schedule.description,
    startDate: schedule.startDate,
    dueDate: schedule.dueDate,
    createdAt: schedule.createdAt,
    updatedAt: schedule.updatedAt,
  }));
}

export const addSchedule = async (newSchedule: Omit<InputSchedule, 'id'>) => {
  const response = await fetch('/api/schedule', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      ...newSchedule,
      id: undefined,
  })
  })
  
  if (!response.ok) {
    const data = await response.json() as unknown;
    throw new Error('Error adding schedule')
  }

  const schedule = await response.json() as unknown;

  return schedule;
}

export const updateSchedule = async (schedule: InputSchedule) => {

  const response = await fetch(`/api/schedule/${schedule.id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schedule) 
  })

  if (!response.ok) {
    const res = await response.json() as unknown;
    const { error : { issues } } = res;
    console.log('issues', issues);
    const msg = issues.map((issue: { path: string[]; message: string; }) => {
      return `${issue.path.join(',')}: ${issue.message}`
    });
    
    throw new Error(msg.join('\n'));
  }

  return await response.json() as unknown;
}

export const deleteSchedule = async (id: string) => {
  const response = await fetch(`/api/schedule/${id}`, {
    method: 'DELETE'
  })

  if (!response.ok) {
    throw new Error('Error deleting schedule')
  }
}
