import React, { useEffect, useState } from "react";
import BlogPostPreview from "../components/BlogPost";
import { fetchPosts } from "../api/blogs";
import { useQuery } from "react-query";
import Card from "../components/Card";

interface BlogPost {
  id: number;
  title: string;
  excerpt: string;
}

interface SearchPageProps {
  blogPosts: BlogPost[];
}

const SearchPage: React.FC<SearchPageProps> = ({ blogPosts }) => {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const { data: posts } = useQuery(["schedules", searchTerm], () =>
    fetchPosts(searchTerm),
  );
  const filteredPosts = posts
    ? posts?.filter((post) =>
        post.title.toLowerCase().includes(searchTerm.toLowerCase()),
      )
    : [];

  useEffect(() => {}, [searchTerm]);

  return (
    <Card>
      <h1>Blog Search</h1>
      <input
        type="text"
        placeholder="Search..."
        className="my-2"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <div>
        {filteredPosts.map((post) => (
          <BlogPostPreview key={post.id} post={post} />
        ))}
      </div>
    </Card>
  );
};

export default SearchPage;
