import type { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '@/server/db';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'GET') {
    const { s } = req.query;
    const posts = await prisma.post.findMany({
      where: s ? {
        OR: [
          {
            title: {
              contains: s as string
            }
          },
          {
            content: {
              contains: s as string
            }
          }
        ]
      }: {},
      orderBy: {
        id: 'desc'
      }
    });
    res.status(200).json({posts});
  }
};


export default handler;

