import type { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '@/server/db';
import { inputSchedule } from '../../../dto/schedule.dto';
import { type Prisma } from '@prisma/client';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'DELETE') {
    await prisma.schedule.delete({
      where: { id: req.query.id as string }
    }).catch((e: Prisma.PrismaClientKnownRequestError) => {
      console.log('error', e);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      res.status(400).json({ error: e.message });
    });
    res.status(200).end();
  }
  if (req.method === 'PUT') {
    const valid = inputSchedule.safeParse(req.body);
    
    if (!valid.success) {
      res.status(400).json({ error: valid.error });
      return;
    }
    const { title, description, startDate, dueDate } = valid.data;
    const schedule = await prisma.schedule.update({
      where: { id: req.query.id as string },
      data: {
        title: title,
        description: description,
        startDate: startDate ? new Date(startDate): null,
        dueDate: dueDate ? new Date(dueDate): null,
      }
    });
    res.status(200).json({ schedule })
  }
}


export default handler;
