import type { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '@/server/db';
import { inputSchedule } from '../../../dto/schedule.dto';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'GET') {
    const schedules = await prisma.schedule.findMany({
      orderBy: {
        id: 'desc'
      }
    });
    res.status(200).json({schedules});
  }
  if (req.method === 'POST') {
    const valid = inputSchedule.safeParse(req.body);
    if (!valid.success) {
      res.status(400).json({ error: valid.error });
      return;
    }
    const { title, description, startDate, dueDate } = valid.data;
    const schedule = await prisma.schedule.create({
      data: {
        title: title,
        description: description,
        startDate: startDate,
        dueDate: dueDate,
      }
    });
    res.status(200).json({schedule});
  }
};


export default handler;

