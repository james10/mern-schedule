import { z } from "zod";

export const inputSchedule = z.object({
  id: z.string().optional().nullable(),
  title: z.string(),
  description: z.string(),
  startDate: z.string().or( z.date() ).transform( arg => new Date( arg ) ).optional().nullable(),
  dueDate: z.string().or( z.date() ).transform( arg => new Date( arg ) ).optional().nullable(),
});

export const newSchedule : z.infer<typeof inputSchedule> = {
  id: null,
  title: '',
  description: '',
  startDate: null,
  dueDate: null,
};
