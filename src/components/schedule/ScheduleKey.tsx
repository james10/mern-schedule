import { Schedule } from "@prisma/client";
import { useState } from "react";

interface Props {
  schedule: Schedule & { startDate: string | null; dueDate: string | null };
  onDelete: (id: string) => void;
  onEdit: (schedule: Schedule) => void;
}

function ScheduleKey({ schedule, onDelete, onEdit }: Props) {
  const handleDelete = () => {
    onDelete(schedule.id);
  };

  const [form, setForm] = useState({
    title: schedule.title,
    startDate: schedule.startDate ?? null,
    dueDate: schedule.dueDate ?? null,
  });

  return (
    <div className="flex gap-2">
      <h3>{schedule.title || "-"}</h3>
      {schedule?.startDate ? (
        <p>Start on {schedule.startDate?.substring(0, 16)}</p>
      ) : null}
      {schedule?.dueDate ? (
        <p>Due on {schedule.dueDate?.substring(0, 16)}</p>
      ) : null}

      <button onClick={handleDelete}>Delete</button>
      <button onClick={() => onEdit(schedule)}>Edit</button>
    </div>
  );
}

export default ScheduleKey;
