import { useState } from "react";
import type { InputSchedule } from "../../api/schedule";
import Button from "../form/Button";

interface Props {
  schedule: InputSchedule;
  onAdd: (schedule: Omit<InputSchedule, "id">) => void;
  onUpdate: (schedule: InputSchedule) => void;
  onCancel: () => void;
  error?: string | null;
}

function ScheduleForm({ onAdd, onUpdate, onCancel, schedule, error }: Props) {
  const [form, setForm] = useState<InputSchedule>({
    id: schedule.id,
    title: schedule.title,
    description: schedule.description,
    startDate: schedule.startDate,
    dueDate: schedule.dueDate,
  });

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!form.id) {
      onAdd(form);
      return;
    }
    onUpdate({
      ...form,
    });
  };

  return (
    <form
      className="w-96 rounded-lg bg-white p-10 shadow"
      onSubmit={handleSubmit}
    >
      {error ? <p className="mb-4 text-red-500">{error}</p> : null}
      <div className="mb-4">
        <input
          type="text"
          name="title"
          className="w-full rounded border p-3 outline-none "
          placeholder="Title"
          value={form.title}
          onChange={(event) =>
            setForm((prev) => ({ ...prev, title: event.target.value }))
          }
        />
      </div>

      <div className="mb-4">
        <input
          type="text"
          name="startDate"
          className="w-full rounded border p-3 outline-none "
          placeholder="Start date"
          value={form.startDate?.toString() ?? ""}
          onChange={(event) =>
            setForm((prev) => ({
              ...prev,
              startDate: event.target.value.toString(),
            }))
          }
        />
      </div>

      <div className="mb-4">
        <input
          type="text"
          name="dueDate"
          className="w-full rounded border p-3 outline-none "
          placeholder="Due date"
          value={form.dueDate?.toString() ?? ""}
          onChange={(event) =>
            setForm((prev) => ({
              ...prev,
              dueDate: event.target.value.toString(),
            }))
          }
        />
      </div>

      <div>
        <Button type="submit" className="my-2 w-full">
          Save
        </Button>
        <Button
          type="button"
          className="w-full bg-gray-200"
          onClick={() => onCancel()}
        >
          Cancel
        </Button>
      </div>
    </form>
  );
}

export default ScheduleForm;
