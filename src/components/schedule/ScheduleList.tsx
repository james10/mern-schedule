import { useQuery, useMutation, useQueryClient } from "react-query";
import ScheduleForm from "./ScheduleForm";
import ScheduleKey from "./ScheduleKey";
import {
  fetchSchedules,
  addSchedule,
  updateSchedule,
  deleteSchedule,
  InputSchedule,
} from "../../api/schedule";
import { Schedule } from "@prisma/client";
import { useState } from "react";
import { newSchedule } from "../../dto/schedule.dto";
import Card from "../Card";
import Button from "../form/Button";

export default function Schedules() {
  const { data: schedules } = useQuery("schedules", fetchSchedules);
  const queryClient = useQueryClient();
  const [editSchedule, setEditSchedule] = useState<InputSchedule | null>(null);

  const addMutation = useMutation(addSchedule, {
    onSuccess: () =>
      queryClient
        .invalidateQueries("schedules")
        .then(() => setEditSchedule(null)),
  });

  const deleteMutation = useMutation(deleteSchedule, {
    onSuccess: () =>
      queryClient
        .invalidateQueries("schedules")
        .then(() => setEditSchedule(null)),
  });

  const updateMutation = useMutation(updateSchedule, {
    onSuccess: () =>
      queryClient
        .invalidateQueries("schedules")
        .then(() => setEditSchedule(null)),
  });
  return (
    <>
      <Card>
        {editSchedule ? (
          <ScheduleForm
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            error={
              (addMutation.error?.message as string) ||
              (updateMutation.error?.message as string)
            }
            schedule={editSchedule}
            onAdd={addMutation.mutate}
            onUpdate={updateMutation.mutate}
            onCancel={() => setEditSchedule(null)}
            // error={addMutation.error?.message || updateMutation.error?.message}
          />
        ) : null}
        {!editSchedule ? (
          <Button onClick={() => setEditSchedule(newSchedule)}>
            Add Schedule
          </Button>
        ) : null}
      </Card>

      <Card>
        {schedules
          ? schedules.map((schedule: Schedule) => (
              <ScheduleKey
                key={schedule.id}
                schedule={schedule}
                onEdit={() => setEditSchedule(schedule)}
                onDelete={(id) => {
                  if (
                    confirm("Are you sure you want to delete this schedule?")
                  ) {
                    deleteMutation.mutate(id);
                  }
                }}
              />
            ))
          : null}
      </Card>
    </>
  );
}
