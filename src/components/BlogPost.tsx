import { Post } from "@prisma/client";
import React from "react";

interface BlogPostPreviewProps {
  post: Post;
}

const BlogPostPreview: React.FC<BlogPostPreviewProps> = ({ post }) => {
  return (
    <div className="blog-post-preview mb-1">
      <h2>{post.title}</h2>
      <p>{post.content}</p>
    </div>
  );
};

export default BlogPostPreview;
