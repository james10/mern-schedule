interface CardProps {
  children: React.ReactNode;
  className?: string;
  title?: string;
}

const Card: React.FC<CardProps> = ({ children, className = "", title }) => {
  return (
    <div className={`overflow-hidden rounded-lg bg-white shadow ${className}`}>
      {title && (
        <div className="border-b p-3">
          <h5 className="font-bold">{title}</h5>
        </div>
      )}

      <div className="p-5">{children}</div>
    </div>
  );
};

Card.defaultProps = {
  title: undefined,
};

export default Card;
