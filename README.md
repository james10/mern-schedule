# Part 5: Full-Stack Application

* Create a full-stack MERN application that allows users to create, edit, and delete schedules, with the post data stored in a MongoDB collection.

* Write a function that validates user input for a MERN stack application and returns an error message if the input is invalid.

* Create a feature that allows users to search for blog posts by keyword, with the search functionality implemented on both the client and server sides.

## Setup

copy .env.example and paste as .env

then install packages:

```
npm install
```

setup db
```
copy .env to root of project
npx prisma generate
npx prisma db push
npx prisma db seed
```

## running

```
npm run dev
```
