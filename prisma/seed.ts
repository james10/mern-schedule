import { PrismaClient } from "@prisma/client";

export const prisma = new PrismaClient({});

const seed = async () => {
  const author = await prisma.user.create({
    data: {
      firstName: 'Alice',
      lastName: 'Smith',
      email: 'alice@example.com',
    }
  })
  const authorId = author.id;

  await prisma.post.create({
    data: {
      authorId,
      title: 'My first post',
      content: 'Hello world!',
      published: true,
    }
  });

  await prisma.post.create({
    data: {
      authorId,
      title: 'My second post',
      content: 'Hello world!',
      published: true,
    }
  });

  await prisma.post.create({
    data: {
      authorId,
      title: 'My 3rd post',
      content: 'Hello world!',
      published: true,
    }
  });
}

seed();